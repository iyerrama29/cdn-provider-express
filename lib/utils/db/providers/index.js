/*!
 * contentstack-express
 * copyright (c) Built.io Contentstack
 * MIT Licensed
 */

'use strict';

/**
 * Module Dependencies.
 */

var config = require('./../../config');

var dataStore = function () {
	const provider = config.get('storage').provider;
    var cache = config.get('cache'),
        storageType = (provider === "FileSystem") ? ((cache) ? "nedb" : "FileSystem") : provider;
    try {
        return require('./' + storageType);
    } catch (e) {
        console.error("Error in datastore loading ...", e);
    }
};

module.exports = dataStore();